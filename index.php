<?php

    $lottos = [
        '1min'  => [
            ['name'=>'GAMMA 3', 'sub-name'=>'EX', 'point'=>'jisuk3m', 'image'=>'1min_1_gamma_3_ex.png', 'serial'=>13227, 'interval'=>75, 'color'=>'#28b3ab', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Gamma3/Express'],
            ['name'=>'PENTA 5', 'sub-name'=>'EX', 'point'=>'cqscm', 'image'=>'1min_2_penta_5_ex.png', 'serial'=>23227, 'interval'=>75, 'color'=>'#6f27f9', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Penta5/Express'],
            ['name'=>'INFINITY 8', 'sub-name'=>'EX', 'point'=>'jisuklsfm', 'image'=>'1min_3_infinity_8_ex.png', 'serial'=>33227, 'interval'=>75, 'color'=>'#f70a8c', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Infinity8/Express'],
            ['name'=>'ROLLING 10', 'sub-name'=>'EX', 'point'=>'jism', 'image'=>'1min_6_rolling_10_ex.png', 'serial'=>43227, 'interval'=>75, 'color'=>'#0062f6', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Rolling10/Express'],
            ['name'=>'CANNON 20', 'sub-name'=>'EX', 'point'=>'jisuk8m', 'image'=>'1min_4_cannon_20_ex.png', 'serial'=>53227, 'interval'=>75, 'color'=>'#ff801c', 'ext-color'=>'#ffb67d', 'url'=>'https://demo.maxgatesoftware.com/#/Cannon20/Express'],
            ['name'=>'LUCKY 5', 'sub-name'=>'EX', 'point'=>'jisu11x5m', 'image'=>'1min_5_lucky_5_ex.png', 'serial'=>63227, 'interval'=>75, 'color'=>'#2a571b', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Lucky5/Express'],
        ],
        '3min'  => [
            ['name'=>'GAMMA 3', 'sub-name'=>'', 'point'=>'jisuk3m3', 'image'=>'3min_1_gamma_3.png', 'serial'=>11633, 'interval'=>185, 'color'=>'#28b3ab', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Gamma3/Regular'],
            ['name'=>'PENTA 5', 'sub-name'=>'', 'point'=>'cqscm3', 'image'=>'3min_2_penta_5.png', 'serial'=>21633, 'interval'=>185, 'color'=>'#6f27f9', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Penta5/Regular'],
            ['name'=>'INFINITY 8', 'sub-name'=>'', 'point'=>'jisuklsfm3', 'image'=>'3min_3_infinity_8.png', 'serial'=>31633, 'interval'=>185, 'color'=>'#f70a8c', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Infinity8/Regular'],
            ['name'=>'ROLLING 10', 'sub-name'=>'', 'point'=>'jism3', 'image'=>'3min_6_rolling_10.png', 'serial'=>41633, 'interval'=>185, 'color'=>'#0062f6', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Rolling10/Regular'],
            ['name'=>'CANNON 20', 'sub-name'=>'', 'point'=>'jisuk8m3', 'image'=>'3min_4_cannon_20.png', 'serial'=>51633, 'interval'=>185, 'color'=>'#ff801c', 'ext-color'=>'#ffb67d', 'url'=>'https://demo.maxgatesoftware.com/#/Cannon20/Regular'],
            ['name'=>'LUCKY 5', 'sub-name'=>'', 'point'=>'jisu11x5m3', 'image'=>'3min_5_lucky_5.png', 'serial'=>61633, 'interval'=>185, 'color'=>'#2a571b', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Lucky5/Regular'],
        ],
        '5min'  => [
            ['name'=>'PENTA 5', 'sub-name'=>'CLASSIC', 'point'=>'cqscm5', 'image'=>'5min_1_penta_5_classic.png', 'serial'=>21888, 'interval'=>305, 'color'=>'#6f27f9', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Penta5/Classic'],
            ['name'=>'SURFING 10', 'sub-name'=>'CLASSIC', 'point'=>'jism5', 'image'=>'img_surfing-classic.png', 'serial'=>41888, 'interval'=>305, 'color'=>'#0062f6', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Surfing10/Classic'],
            ['name'=>'LUCKY 7', 'sub-name'=>'CLASSIC', 'point'=>'jisu49x7m5', 'image'=>'5min_2_lucky_7_classic.png', 'serial'=>61633, 'interval'=>305, 'color'=>'#239a0b', 'ext-color'=>''],
        ],
        'daily' => [
            ['name'=>'LUCKY 7', 'sub-name'=>'DAILY', 'point'=>'Lucky7Daily', 'image'=>'img_lucky-7-daily.png', 'serial'=>61633, 'interval'=>86400, 'color'=>'#6f27f9', 'ext-color'=>'', 'url'=>'https://demo.maxgatesoftware.com/#/Lucky7/Daily']
        ]
    ];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title>Lotto</title>
</head>
<body>
    <div class="lotto-container">
        <div class="banner"><img src="img/banner.jpg" /></div>
        <h3>UK Keno Results</h3>
        <div class="tab-header">
            <ul>
                <li class="active" rel="1min"><div>UK KENO <span>EX</span><span class="minspan">-1MIN</span></div></li>
                <li rel="3min">UK KENO-3MIN</li>
                <li rel="5min">UK KENO <span>CLASSIC</span><span>-5MIN</span></li>
                <li rel="daily">UK KENO <span>DAILY</span><span></span></li>
            </ul>
            <div class="tab-header-border">
                <div class="active" rel="1min"><div class="flag"></div></div>
                <div rel="3min"><div class="flag"></div></div>
                <div rel="5min"><div class="flag"></div></div>
                <div rel="daily"><div class="flag"></div></div>
            </div>
        </div>
        <div class="tab-container">
            <div class="tab active" rel="1min">
                <div class="accordions">
                <?php foreach ($lottos['1min'] as $lotto) { ?>
                    <div class="accordion">
                        <h4 data-ext-color="<?= $lotto['ext-color'] ?>" data-color="<?= $lotto['color'] ?>" data-val="<?= $lotto['point'] ?>" data-serial="<?= $lotto['serial'] ?>" data-interval="<?= $lotto['interval'] ?>"><?=$lotto['name']?> <span><?=$lotto['sub-name']?></span></h4>
                        <div class="accordion-body open">
                            <div class="content latest">
                                <div class="float icon"><img src="img/<?= $lotto['image'] ?>" alt="<?= $lotto['name']?>" /></div>
                                <div class="float date-amount">
                                    <div class="draw-date"></div>
                                    <div class="next-draw-timer">Next Draw: <span></span></div>
                                </div>
                                <div class="float result-set"></div>
                                <div class="more"><img class="more-img" src="img/more.svg"/><br>
                                <?php if (!empty($lotto['url'])) { ?>
                                    <a href="<?= $lotto['url'] ?>" target="_blank"><img src="img/play_for_fun.svg"></a>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="past-results">
                                <div class="date">
                                    <input class="datepicker" readonly></input>
                                    <i class="cal"></i>
                                </div>
                                <div class="content" data-batch="1">
                                    
                                </div>
                                <div class="load-more-containter">
                                    <button class="load-more">Load More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                </div>
            <div class="tab" rel="3min">
            <div class="accordions">
            <?php foreach ($lottos['3min'] as $lotto) { ?>
            <div class="accordion">
                <h4 data-ext-color="<?= $lotto['ext-color'] ?>" data-color="<?= $lotto['color'] ?>" data-val="<?= $lotto['point'] ?>" data-serial="<?= $lotto['serial'] ?>" data-interval="<?= $lotto['interval'] ?>"><?=$lotto['name']?> <span><?=$lotto['sub-name']?></span></h4>
                <div class="accordion-body open">
                    <div class="content latest">
                        <div class="float icon"><img src="img/<?= $lotto['image'] ?>" alt="<?= $lotto['name']?>" /></div>
                        <div class="float date-amount">
                            <div class="draw-date"></div>
                            <div class="next-draw-timer">Next Draw: <span></span></div>
                        </div>
                        <div class="float result-set"></div>
                        <div class="more"><img class="more-img" src="img/more.png"/><br>
                        <?php if (!empty($lotto['url'])) { ?>
                            <a href="<?= $lotto['url'] ?>" target="_blank"><img src="img/play_for_fun.png"></a>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="past-results">
                        <div class="date">
                            <input class="datepicker" readonly></input>
                            <i class="cal"></i>
                        </div>
                        <div class="content" data-batch="1">
                            
                        </div>
                        <div class="load-more-containter">
                            <button class="load-more">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
            </div>
            <div class="tab" rel="5min">
            <div class="accordions">
            <?php foreach ($lottos['5min'] as $lotto) { ?>
            <div class="accordion">
                <h4 data-ext-color="<?= $lotto['ext-color'] ?>" data-color="<?= $lotto['color'] ?>" data-val="<?= $lotto['point'] ?>" data-serial="<?= $lotto['serial'] ?>" data-interval="<?= $lotto['interval'] ?>"><?=$lotto['name']?> <span><?=$lotto['sub-name']?></span></h4>
                <div class="accordion-body open">
                    <div class="content latest">
                        <div class="float icon"><img src="img/<?= $lotto['image'] ?>" alt="<?= $lotto['name']?>" /></div>
                        <div class="float date-amount">
                            <div class="draw-date"></div>
                            <div class="next-draw-timer">Next Draw: <span></span></div>
                        </div>
                        <div class="float result-set"></div>
                        <div class="more"><img class="more-img" src="img/more.png"/><br>
                        <?php if (!empty($lotto['url'])) { ?>
                            <a href="<?= $lotto['url'] ?>" target="_blank"><img src="img/play_for_fun.png"></a>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="past-results">
                        <div class="date">
                            <input class="datepicker" readonly></input>
                            <i class="cal"></i>
                        </div>
                        <div class="content" data-batch="1">
                            
                        </div>
                        <div class="load-more-containter">
                            <button class="load-more">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
            </div>
            <div class="tab" rel="daily">
            <div class="accordions">
            <?php foreach ($lottos['daily'] as $lotto) { ?>
            <div class="accordion">
                <h4 data-ext-color="<?= $lotto['ext-color'] ?>" data-color="<?= $lotto['color'] ?>" data-val="<?= $lotto['point'] ?>" data-serial="<?= $lotto['serial'] ?>" data-interval="<?= $lotto['interval'] ?>"><?=$lotto['name']?> <span><?=$lotto['sub-name']?></span></h4>
                <div class="accordion-body open">
                    <div class="content latest">
                        <div class="float icon"><img src="img/<?= $lotto['image'] ?>" alt="<?= $lotto['name']?>" /></div>
                        <div class="float date-amount">
                            <div class="draw-date"></div>
                            <div class="next-draw-timer">Next Draw: <span></span></div>
                        </div>
                        <div class="float result-set"></div>
                        <div class="more"><img class="more-img" src="img/more.png"/><br>
                        <?php if (!empty($lotto['url'])) { ?>
                            <a href="<?= $lotto['url'] ?>" target="_blank"><img src="img/play_for_fun.png"></a>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="past-results">
                        <div class="date">
                            <input class="datepicker" readonly></input>
                            <i class="cal"></i>
                        </div>
                        <div class="content" data-batch="1">
                            
                        </div>
                        <div class="load-more-containter">
                            <button class="load-more">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
            </div>
        </div>
    </div>
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
    <script src="js/datepicker.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>