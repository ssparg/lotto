<?php

date_default_timezone_set('Africa/Johannesburg');
function get($url) {
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_HEADER, FALSE); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $results = curl_exec($ch);
    curl_close($ch); 
    if($results == false) {
        echo json_encode(array('error' => 'Unable to get results'));
        exit;
    } else {
        return json_decode($results);
    }
}

function parseDate($date) {
    $date = new DateTime($date, new DateTimeZone('Asia/Hong_Kong'));
    $date->setTimezone(new DateTimeZone('Europe/London'));
    return $date->format('dS F Y, H:i:s');
}

function calculateNextDrawTime($drawTime, $interval) {
    return date('Y-m-d H:i:s', strtotime($drawTime . ' +'.$interval.' SECOND'));
}

function calculateNextDrawTimeDifference($currentDrawTime, $nextDrawTime) {
    $currentDrawTime = new DateTime($currentDrawTime);
    $nextDrawTime = new DateTime($nextDrawTime);
    $diff = $currentDrawTime->diff($nextDrawTime);
    return ($diff->h * 60 * 60) + ($diff->i * 60) + $diff->s;
}

function calculatePeriodNumber($periodNo, $drawTime, $initialDrawNumber, $firstDrawDate = '2019-03-13') {
    $now = new DateTime($drawTime);
    $firstDraw = new DateTime($firstDrawDate);
    $diff = $now->diff($firstDraw);
    $days = $diff->days;
    return $initialDrawNumber + $days . ' ' . substr($periodNo, 8);
}