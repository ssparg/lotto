function ajax_post(data, callback) {
    $.ajax({
        url: 'lotto.php',
        method: 'POST',
        data: data,
        dataType: 'json'
    }).done(function(d){
        if (typeof callback == 'function') {
            callback(d);
        }
    });
}

function build_lotto(accordionBody, point, serial, interval, color, ext_color) {  
    ajax_post({point: point, interval: interval, serial: serial}, function(d){
        var oldResultSet = new Array();
        var oldResults = accordionBody.find('.latest');
        var oldDate = oldResults.find('.draw-date');
        var drawNo = oldDate.find('span').html();
        var addToHistory = false;
        
        if (typeof drawNo != 'undefined') {
            drawNo = drawNo.split(' ');
            drawNo = drawNo.pop().trim();
            if (drawNo == d.number) {
                build_lotto(accordionBody, point, serial, interval, color, ext_color);
                return;
            }
        }
        
        var pastDate = accordionBody.find('.past-results .result-row').first().find('.date-amount');

        if (pastDate.length > 0) {
            var broken = pastDate.html().split(',');
            if (broken.length) {
                var newDate = d.date.split(',');
                
                if (newDate[0] == broken[0]) {
                    addToHistory = true;
                }
            }
        }
        if (accordionBody.find('.past-results').hasClass('open') && addToHistory) {
            
            $.each(oldResults.find('.result-set span'), function () {
                oldResultSet.push($(this).html());
            });
            if (!comepareResults(d.balls, oldResultSet)) {
                var newPastResult = '<div class="result-row"><div class="float icon"></div><div class="float date-amount">'+oldDate.html()+'</div><div class="float result-set">'+oldResults.find('.result-set').html()+'</div></div>';
                accordionBody.find('.past-results .content').prepend(newPastResult);
            }
        }
        
        var html = '';
        if (typeof d.error == 'undefined') {
            html = buildBalls(d.balls, ext_color, color, d.extcode, point);
            
            accordionBody.find('.latest .date-amount .draw-date').html(d.date+', <span><strong>'+d.new_period+'</strong></span>');
        } else {
            html += '<div class="error">'+d.error+'</div>';
        }

        if (typeof d.next != 'undefined') {            
            startTimer(point, d.diff, accordionBody.find('.latest .date-amount .next-draw-timer span'));
            
            accordionBody.closest('.accordion').find('h4').attr('data-next', d.next);
        }

        accordionBody.find('.latest .result-set').html(html);
        
        setTimeout(function(){
            accordionBody.closest('.accordion').find('h4').removeAttr('data-updating');
            accordionBody.find('.result-set span').removeClass('fa-spin-custom');
        }, 1000);
        
    });
}

function buildBalls(balls, ext_color, color, extcode, point) {
    var html = '';
    var lucky7Classic = [
        '#bb2b39', '#bb2b39', '#2778d3', '#2778d3', '#239a0b', '#239a0b', '#bb2b39', '#bb2b39', '#2778d3', '#2778d3',
        '#239a0b', '#bb2b39', '#bb2b39', '#2778d3', '#2778d3', '#239a0b', '#239a0b', '#bb2b39', '#bb2b39', '#2778d3',
        '#239a0b', '#239a0b', '#bb2b39', '#bb2b39', '#2778d3', '#2778d3', '#239a0b', '#239a0b', '#bb2b39', '#bb2b39',
        '#2778d3', '#239a0b', '#239a0b', '#bb2b39', '#bb2b39', '#2778d3', '#2778d3', '#239a0b', '#239a0b', '#bb2b39',
        '#2778d3', '#2778d3', '#239a0b', '#239a0b', '#bb2b39', '#bb2b39', '#2778d3', '#2778d3', '#239a0b' 
    ];
    for (var index = 0; index < balls.length; index++) {
        var ballClass = 'ball';
        if ((point == 'jisu49x7m5' || point == 'Lucky7Daily') && (index == (balls.length - 1))) {
            ballClass += ' plus ball-last';
        }
        html += '<span class="'+ballClass+'" style="background-color:'+ (point == 'jisu49x7m5' || point == 'Lucky7Daily' ? lucky7Classic[balls[index] - 1] : color) +'">'+balls[index]+'</span>';
        
    }
    if (typeof extcode != 'undefined') {
        html += '<span class="ball ball-last extcode" style="background-color:'+ext_color+'">'+extcode+'</span>';
    }

    return html;
}

function comepareResults(newSet, oldSet) {   
    return newSet.equals(oldSet);
}

function buildPastResults(date, serial, point, content, batch, color, ext_color) {
    content.closest('.past-results').find('.load-more').attr('disabled', 'disabled');
    ajax_post({point: point, date: date, batch, serial: serial}, function(d) {
        var html = '';
        if (typeof d.error == 'undefined') {
            for (var index = 0; index < d.length; index++) {
                html += '<div class="result-row"><div class="float icon"></div><div class="float date-amount">'+d[index].date+', <span><strong>'+d[index].new_period+'</strong></span></div><div class="float result-set">';
                html += buildBalls(d[index].balls, ext_color, color, d[index].extcode, point)
                html += '</div><div class="more"></div>&nbsp;</div>';
            }
        } else {
            html += '<div class="error">'+d.error+'</div>';
        }

        if (d.length == 0) {
            content.closest('.past-results').find('.load-more').attr('disabled', 'disabled');
        } else {
            content.closest('.past-results').find('.load-more').removeAttr('disabled');
        }

        content.append(html);
        content.attr('data-batch', parseInt(batch) + 1);
    });
}

function startTimer(point, duration, display) {
    var timer = duration, hours, minutes, seconds;
    
    var i = setInterval(function () {
        if (point == 'Lucky7Daily') {
            hours = Math.floor(timer / 3600);
            hours = hours < 10 ? "0" + hours : hours;
            minutes = Math.floor(timer % 3600 / 60);
            seconds = Math.floor(timer % 3600 % 60);
        }
        minutes = Math.floor(timer % 3600 / 60);
        seconds = Math.floor(timer % 3600 % 60);
        
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.html(point == 'Lucky7Daily' ? hours + ":" + minutes + ":" + seconds : minutes + ":" + seconds);
        
        if (timer > 0) {
            --timer;
        } else {
            clearInterval(i);
        }
    }, 1000);
}

function today() {
    var date = new Date();    
    var year = date.getFullYear();
    var month = String(date.getMonth()+1).padStart(2, 0);
    var day = String(date.getDate()).padStart(2, 0);
    return year + '-' + month + '-' + day;
}

function toUKPeriodNo(PeriodNo, datetime, BeginPeriodNo) {
    var now = new Date(datetime);
    var dateBegin = new Date("2019-03-13 00:00:00");
    var dateDiff = now.getTime() - dateBegin.getTime();
    var dayDiff = Math.floor(parseInt(dateDiff) / (24 * 3600 * 1000));
    var UKPeriodNo = parseInt(BeginPeriodNo) + parseInt(dayDiff);
    return UKPeriodNo + " " + (PeriodNo.substr(8));
}

if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

$(document).ready(function(){

    $('.tab-header ul li').on('click', function () {
        if ($(this).hasClass('active')) {
            return;
        }
       var rel = $(this).attr('rel');
       $('.tab-header ul li').removeClass('active');
       $(this).addClass('active');
       $('.tab-header .tab-header-border div').removeClass('active');
       $('.tab-header .tab-header-border div[rel="'+rel+'"]').fadeIn().addClass('active');
       $('.tab-container .tab').fadeOut().removeClass('active');
       $('.tab-container .tab[rel="'+rel+'"]').fadeIn().addClass('active');
    });

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoHide: true,
        endDate: new Date()
    });

    $('.datepicker').on('pick.datepicker', function () {
        var date = $(this).datepicker('getDate', true);
        var serial = $(this).closest('.accordion').find('h4').attr('data-serial');
        var point = $(this).closest('.accordion').find('h4').attr('data-val');
        var color = $(this).closest('.accordion').find('h4').attr('data-color');
        var ext_color = $(this).closest('.accordion').find('h4').attr('data-ext-color');
        var content = $(this).closest('.past-results').find('.content');
        content.empty();
        content.closest('.past-results').find('.load-more').removeAttr('disabled');
        buildPastResults(date, serial, point, content, 1, color, ext_color);        
      });

    $.each($('.lotto-container .accordion'), function(){
        var accordionBody = $(this).find('.accordion-body');
        var point = $(this).find('h4').attr('data-val');
        var color = $(this).find('h4').attr('data-color');
        var ext_color = $(this).find('h4').attr('data-ext-color');        
        var serial = $(this).find('h4').attr('data-serial');
        var interval = $(this).find('h4').attr('data-interval');
        build_lotto(accordionBody, point, serial, interval, color, ext_color);
        
    });
    $('.lotto-container').on('click', '.accordion h4', function(){
        var accordionBody = $(this).closest('.accordion').find('.accordion-body')
        accordionBody.toggleClass('open');      
    });
    $('.lotto-container').on('click', '.more-img', function(){
        var pastResults = $(this).closest('.accordion-body').find('.past-results');
        var header = $(this).closest('.accordion').find('h4')
        var point = header.attr('data-val');
        var color = header.attr('data-color');
        var ext_color = header.attr('data-ext-color');
        var serial = header.attr('data-serial');
        var content = pastResults.find('.content');
        var batch = content.attr('data-batch');
        var date = today();
        content.closest('.past-results').find('.load-more').removeAttr('disabled');
        buildPastResults(date, serial, point, content, batch, color, ext_color);
        $(this).addClass('less').removeClass('more');
        pastResults.addClass('open');
        $(this).html('Less');
    });
    $('.lotto-container').on('click', '.less', function(){
        var pastResults = $(this).closest('.accordion-body').find('.past-results');
        var content = pastResults.find('.content');
        content.empty();
        pastResults.find('.datepicker').val('');
        content.attr('data-batch', '1');
        content.closest('.past-results').find('.load-more').removeAttr('disabled');
        $(this).removeClass('less').addClass('more');
        pastResults.removeClass('open');
        $(this).html('More');
    });
    $('.lotto-container').on('click', '.load-more', function(){
        var pastResults = $(this).closest('.accordion-body').find('.past-results');
        var header = $(this).closest('.accordion').find('h4')
        var point = header.attr('data-val');
        var serial = header.attr('data-serial');
        var content = pastResults.find('.content');
        var batch = content.attr('data-batch');
        var date = (pastResults.find('.datepicker').val() != '' ? pastResults.find('.datepicker').val() : today());
        var color = header.attr('data-color');
        var ext_color = header.attr('data-ext-color');
        buildPastResults(date, serial, point, content, batch, color, ext_color);
    });

    setTimeout(function(){
        $.each($('.lotto-container .accordion'), function() {
            var that = $(this);
            
            setInterval(function(){
                var next = that.find('.accordion-body .latest .date-amount .next-draw-timer span').html();
                var busy = that.find('h4').attr('data-updating');
                if ((next == '00:00' || next == "00:00:00") && typeof busy == 'undefined') {                    
                    that.find('h4').attr('data-updating', 'true');
                    var accordionBody = that.find('.accordion-body');
                    var point = that.find('h4').attr('data-val');
                    var color = that.find('h4').attr('data-color');
                    var serial = that.find('h4').attr('data-serial');
                    var ext_color = that.find('h4').attr('data-ext-color');
                    var interval = that.find('h4').attr('data-interval');                    
                    if (point == 'jisu49x7m5' && accordionBody.find('.latest .result-set span').length > 0) {
                        return;
                    }
                    accordionBody.find('.latest .result-set span').addClass('fa-spin-custom');
                    build_lotto(accordionBody, point, serial, interval, color, ext_color);
                }
            }, 1000);
        });
    }, 5000);
});