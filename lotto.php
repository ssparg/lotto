<?php

require_once 'functions.php';

$point = $_POST['point'];
$base = 'https://www.uk-wl.net/';
// if ($point == 'Lucky7Daily') {
//     $base = 'https://ukwl.sc369.net/';
// }
$url = !empty($_POST['date']) ? $base.$point.'/key/allday/'.$_POST['date'] : $base.$point.'/key/latest';
$interval = !empty($_POST['interval']) ? $_POST['interval'] : false;
$batch = !empty($_POST['batch']) ? $_POST['batch'] : 1;
$serial = $_POST['serial'];
$results = get($url, empty($_POST['date']), $interval);
$draws = [];
$padResultSets = ['jism', 'jism3', 'jism5'];

if (empty($_POST['date'])) {
    $set = array_shift($results->Data->Results);
    $draws['balls'] = explode(',', $set->OpenCode);
    if (in_array($point, $padResultSets)) {
        array_walk($draws['balls'], function(&$ball){
            $ball = $ball < 10 ? 0 . $ball : $ball;
        });
    }
    if (count($draws['balls']) == 20) {
        sort($draws['balls']);
    }
    // if ($point === 'Lucky7Daily') {
    //     $set->OpenTime = date('Y-m-d H:i:s', strtotime($set->OpenTime . '-1 HOUR'));
    // }
    $draws['date'] = parseDate($set->OpenTime);
    $draws['number'] = substr($set->PeriodNo, 8);
    $draws['new_period'] = calculatePeriodNumber($set->PeriodNo, $set->OpenTime, $serial);
    if (!empty($set->ExtCode)) {
        $draws['extcode'] = $set->ExtCode;
    }
    
    $draws['next'] = calculateNextDrawTime($draws['date'], $interval);
    $draws['diff'] = calculateNextDrawTimeDifference(date('Y-m-d H:i:s', strtotime('-2 HOUR')), $draws['next']);
    $draws['interval'] = $interval;
    
} else {
    $batch = ($batch - 1) * 30;
    $r = array_slice($results->Data->Results, $batch, 30);
    foreach ($r as $set) {
        $balls = explode(',', $set->OpenCode);
        if (in_array($point, $padResultSets)) {
            array_walk($balls, function(&$ball){
                $ball = $ball < 10 ? 0 . $ball : $ball;
                
            });
        }
        if (count($balls) == 20) {
            sort($balls);
        }
        // if ($point === 'Lucky7Daily') {
        //     $set->OpenTime = date('Y-m-d H:i:s', strtotime($set->OpenTime . '-1 HOUR'));
        // }
        $date = parseDate($set->OpenTime);
        $number = substr($set->PeriodNo, 8);
        $new_period = calculatePeriodNumber($set->PeriodNo, $set->OpenTime, $serial);
        if (!empty($set->ExtCode)) {
            $extcode = $set->ExtCode;
            array_push($draws, compact('balls', 'date', 'number', 'extcode', 'new_period'));
        } else {
            array_push($draws, compact('balls', 'date', 'number', 'new_period'));
        }
    }
}

if ($point == 'jisu49x7m5' && (empty($_POST['date']) || (isset($_POST['date']) && $_POST['date'] > '2020-05-07'))) {
    $draws = [];
    $draws['balls'] = [10,14,44,34,04,32,12];
    $draws['date'] = "07th May 2020, 03:19:15";
    $draws['new_period'] = "62054 136";
    $draws['next'] = "00:00";
    $draws['number'] = 136;
    $draws['diff'] = 0;
}

echo json_encode($draws);




